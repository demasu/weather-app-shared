import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApixuService {
  // You can get a free key here: https://www.weatherstack.com/
  private accessKey: string = 'redacted';
  private URL: string;
  private httpClient: HttpClient;

  constructor(private http: HttpClient) {
    this.URL = `http://api.weatherstack.com/current?access_key=${this.accessKey}&query=`;
    this.httpClient = http
  }

  getWeather(location: string, units: string) {
    let url = this.URL + location;
    url += '&units=' + units;
    return this.httpClient.get(url);
  }
}


/**
 * The raw return of the API is like so:
 {
  "request": {
    "type": "City",
    "query": "New York, United States of America",
    "language": "en",
    "unit": "m"
  },
  "location": {
    "name": "New York",
    "country": "United States of America",
    "region": "New York",
    "lat": "40.714",
    "lon": "-74.006",
    "timezone_id": "America/New_York",
    "localtime": "2023-11-06 13:15",
    "localtime_epoch": 1699276500,
    "utc_offset": "-5.0"
  },
  "current": {
    "observation_time": "06:15 PM",
    "temperature": 12,
    "weather_code": 113,
    "weather_icons": [
      "https://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
    ],
    "weather_descriptions": [
      "Sunny"
    ],
    "wind_speed": 4,
    "wind_degree": 147,
    "wind_dir": "SSE",
    "pressure": 1022,
    "precip": 0,
    "humidity": 50,
    "cloudcover": 0,
    "feelslike": 12,
    "uv_index": 3,
    "visibility": 16,
    "is_day": "yes"
  }
}
 */
