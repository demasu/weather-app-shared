import { Pipe, PipeTransform } from '@angular/core';
import { WeatherData } from './weather-data';

@Pipe({
  name: 'customWeather'
})

// request: {
//   unit: string;
// };
// location: {
//   name: string;
//   region: string;
//   localtime_epoch: number;
// };
// current: {
//   observation_time: string;
//   temperature: number;
//   weather_descriptions: string[];
//   wind_speed: number;
//   wind_dir: string;
//   pressure: number;
//   precip: number;
//   humidity: number;
//   feelslike: number;
//   uv_index: number;
//   is_day: string;
// };

export class CustomWeatherPipe implements PipeTransform {

  units = {
    s: {
      temperature: 'Kelvin',
    },
    m: {
      temperature: 'Celsius',
    },
    f: {
      temperature: 'Fahrenheit',
    },
  };

  // transform(value: (number | string | string[]), ...args: unknown[]): unknown {
  transform(value: (number | string | string[]), key: string): string {
    if ( key === 'temperature' ) {
      return `The current temperature is ${value} degrees ${this.getUnit(key)}`;
    }
    return 'thing';
  }

  getUnit(key: string): string {
    if ( key === 'temperature' ) {
      return 'F';
    }
    return '';
  }
}
