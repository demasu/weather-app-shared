export interface WeatherData {
  /**
 * Request contains information pertaining to the request sent.
 */
  request: {
    unit: string;
  };
  location: {
    name: string;
    region: string;
    localtime_epoch: number;
  };
  current: {
    observation_time: string;
    temperature: string;
    weather_icons: string[];
    weather_descriptions: string[];
    wind_speed: string;
    wind_dir: string;
    pressure: string;
    precip: number;
    humidity: number;
    feelslike: string;
    uv_index: number;
    is_day: string;
    weather_code: number;
  };
}

export interface APIOutput {
  request: {
    type: string;
    query: string;
    language: string;
    unit: string;
  },
  location: {
    name: string;
    country: string;
    region: string;
    lat: string;
    lon: string;
    timezone_id: string;
    localtime: string;
    localtime_epoch: number;
    utc_offset: string;
  },
  current: {
    observation_time: string;
    temperature: number;
    weather_code: number;
    weather_icons: string[];
    weather_descriptions: string[];
    wind_speed: number;
    wind_degree: number;
    wind_dir: string;
    pressure: number;
    precip: number;
    humidity: number;
    cloudcover: number;
    feelslike: number;
    uv_index: number;
    visibility: number;
    is_day: string;
  }
}
