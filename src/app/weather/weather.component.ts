import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ApixuService } from '../apixu.service';
import { WeatherData } from '../weather-data';

declare var $: any;

type FormOutput = {
  location: string;
  units: string;
}

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent {
  public weatherSearchForm: FormGroup;
  public weatherData: WeatherData;
  private mbarToInHg: number = 0.02953;

  constructor( private formBuilder: FormBuilder, private ApixuService: ApixuService ) {
    this.weatherSearchForm = new FormGroup({});
    this.weatherData = {} as WeatherData;
  }

  ngOnInit() {
    this.weatherSearchForm = this.formBuilder.group({
      location: [''],
      units: ['f'],
    });
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
    })
  }

  mockResponse(): any {
    return {
      "request": {
        "type": "City",
        "query": "Alvin, United States of America",
        "language": "en",
        "unit": "f"
      },
      "location": {
        "name": "Alvin",
        "country": "United States of America",
        "region": "Texas",
        "lat": "29.424",
        "lon": "-95.244",
        "timezone_id": "America/Chicago",
        "localtime": "2023-10-27 16:49",
        "localtime_epoch": 1698425340,
        "utc_offset": "-5.0"
      },
      "current": {
        "observation_time": "09:49 PM",
        "temperature": 84,
        "weather_code": 116,
        "weather_icons": [
          "https://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
        ],
        "weather_descriptions": [
          "Partly cloudy"
        ],
        "wind_speed": 9,
        "wind_degree": 170,
        "wind_dir": "S",
        "pressure": 1017,
        "precip": 0,
        "humidity": 65,
        "cloudcover": 75,
        "feelslike": 93,
        "uv_index": 7,
        "visibility": 10,
        "is_day": "yes"
      }
    };
  }

  sendToAPIXU(formValues: FormOutput) {
    // let response = this.mockResponse();
    this.ApixuService.getWeather(formValues.location, formValues.units).subscribe((response: any) => {
      let pressureUnit = 'mbar';
      let speedUnit = 'kph';
      let tempUnit = 'C';
      let precipUnit = 'mm';
      if ( response.request.unit === 'f' ) {
        response.current.pressure = response.current.pressure * this.mbarToInHg;
        pressureUnit = 'inHg';
        speedUnit = 'mph';
        tempUnit = 'F';
        precipUnit = 'in';
      }
      else if ( response.request.unit === 's' ) {
        tempUnit = 'K';
      }
      response.current.pressure = response.current.pressure + ' ' + pressureUnit;
      response.current.wind_speed = response.current.wind_speed + ' ' + speedUnit;
      response.current.precip = response.current.precip + ' ' + precipUnit;
      this.weatherData = response as WeatherData;
    });
  }
}
